// -*- C++ -*-
#include "Rivet/Analysis.hh"
#include "Rivet/Projections/FastJets.hh"
#include "Rivet/Projections/FinalState.hh"
#include "Rivet/Projections/IdentifiedFinalState.hh"
#include "Rivet/Projections/VetoedFinalState.hh"
#include "Rivet/Projections/PromptFinalState.hh"
#include "Rivet/Projections/DressedLeptons.hh"

namespace Rivet {

  /// Zcc in pp at 13 TeV
  class Zcc_rivet : public Analysis {
  public:

    /// Constructor
    DEFAULT_RIVET_ANALYSIS_CTOR(Zcc_rivet);

    /// Book histograms and initialise projections before the run
    void init() {
		
	      // Get options
      _mode = 0;
      if ( getOption("LMODE") == "ELEL") _mode = 1;
      if ( getOption("LMODE") == "MUMU") _mode = 2;
	  
      // Define final state |eta| < 5
      const FinalState fs;

      IdentifiedFinalState photon_fs(fs);
      photon_fs.acceptIdPair(PID::PHOTON);
      PromptFinalState photons(photon_fs);

      IdentifiedFinalState el_id(fs);
      el_id.acceptIdPair(PID::ELECTRON);
      PromptFinalState electrons(el_id);

      IdentifiedFinalState mu_id(fs);
      mu_id.acceptIdPair(PID::MUON);
      PromptFinalState muons(mu_id);

      // Kinematic cuts for leptons
      Cut cuts_el = (Cuts::pT > 28*GeV) && (Cuts::abseta < 2.5);
      Cut cuts_mu = (Cuts::pT > 28*GeV) && (Cuts::abseta < 2.5);

      DressedLeptons dressed_electrons(photons, electrons, 0.1, cuts_el);
      declare(dressed_electrons, "DressedElectrons");

      DressedLeptons dressed_muons(photons, muons, 0.1, cuts_mu);
      declare(dressed_muons, "DressedMuons");

      FastJets jets(fs, FastJets::ANTIKT, 0.4, JetAlg::Muons::ALL, JetAlg::Invisibles::DECAY);
      declare(jets, "Jets");
      
//      declare(MissingMomentum(fs), "MET");

      std::vector<double> ZpTbin = {0., 15., 30., 40., 50., 60., 75., 90., 110., 130., 180., 240., 350., 500., 1000.};
      std::vector<double> Zmassbin = {70.,72.,74.,76.,78.,80.,82.,84.,86.,88.,90.,92.,94.,96.,98.,100.,102.,104.,106.,108.,110.};
      std::vector<double> Zabsybin = {0.,0.25,0.5,0.75,1.,1.25,1.5,1.75,2.,2.25,2.5};

      std::vector<double> deltaRbin = {0, 0.4, 0.9, 1.3, 1.8, 2.3, 2.7, 3.1, 3.5, 4.0, 5.0};
      std::vector<double> deltaYbin = {0, 0.4, 0.9, 1.3, 1.8, 2.3, 2.7, 3.1, 3.5, 4.0, 5.0};
      std::vector<double> deltaPhibin = {0,0.2625,0.525,0.7875,1.05,1.3125,1.575,1.8375,2.1,2.3625,2.625,2.8875,3.15};
      std::vector<double> jpTbin = {17., 20., 24., 30., 36., 43., 60., 80., 100., 130., 170., 230., 330., 500., 900.};

	  std::vector<double> xFbins = {0.,0.01,0.02,0.03,0.04,0.05,0.06,0.08,0.10,0.14,0.18,0.22,0.30,0.5,1};

      //std::vector<double> jetabin = [500,-4.5,4.5];
      //std::vector<double> lep_pt = [900, 0, 900]
      //std::vector<double> lep_eta = [60,-3,3]

      book(_h["ZMass"],"ZMass" , Zmassbin);

      book(_h["NcJets_incl"],"NcJets_incl" , 8, 1,9);
      book(_h["NcJets_excl"],"NcJets_excl" , 8, 1,9);
      book(_h["NcJets_loDR"],"NcJets_loDR" , 8, 1,9);
      book(_h["NcJets_hiDR"],"NcJets_hiDR" , 8, 1,9);

      book(_h["leadingJet_pT"], "j0pT",    jpTbin);
      book(_h["closestJet_pT"], "clJetpT", jpTbin);
      book(_h["2ndJetJet_pT"],  "j1pT",    jpTbin);

      book(_h["2ndcJet_pT"], "2ndcJet_pT", jpTbin);
      book(_h["i2c_2ndcm"], "i1c_2ndcm", jpTbin);

      book(_h["ZJ_pTRatio"],      "ZJpT"     , 30, 0, 3);
      book(_h["ZJ_pTRatio_loDR"], "ZJpT_loDR", 30, 0, 3);
      book(_h["ZJ_pTRatio_hiDR"], "ZJpT_hiDR", 30, 0, 3);

      book(_h["DR"], "DR", deltaRbin);
      book(_h["HT"], "HT", jpTbin);

      book(_h["mjj"],      "mjj",      jpTbin);
      book(_h["mjj_loDR"], "mjj_loDR", jpTbin);
      book(_h["mjj_hiDR"], "mjj_hiDR", jpTbin);

      book(_h["jjDPhi"],      "dPhijj",     deltaPhibin);
      book(_h["jjDPhi_loDR"], "dPhijj_loDR", deltaPhibin);
      book(_h["jjDPhi_hiDR"], "dPhijj_hiDR", deltaPhibin);
      
      book(_h["ZpTjet"], "ZpTjet", ZpTbin);
      
      /////////////////////////////////////////////////////////////////

      book(_h["i1c_ZpT"], "i1c_ZpT", ZpTbin);
      book(_h["i1c_ZY"],"i1c_ZY", deltaYbin);;
      book(_h["i1c_dPhiZc"],"i1c_dPhiZc", deltaPhibin);
      book(_h["i1c_dRZc"],"i1c_dRZc", deltaRbin);
      book(_h["i1c_dYZc"],"i1c_dYZc", deltaYbin);
      book(_h["i1c_MZc"],"i1c_MZc", Zmassbin);
      book(_h["i1c_cpT"],"i1c_cpT",  jpTbin);
      book(_h["i1c_cm"],"i1c_cm", jpTbin);
      book(_h["i1c_cY"],"i1c_cY",deltaYbin);
      book(_h["i1c_Xf"],"i1c_Xf", xFbins);
      book(_h["i2c_ZpT"],"i2c_ZpT", ZpTbin);
      book(_h["i2c_dPhicc"],"i2c_dPhicc", deltaPhibin);
      book(_h["i2c_dRcc"],"i2c_dRcc",deltaRbin);
      book(_h["i2c_dYcc"],"i2c_dYcc", deltaYbin);
      book(_h["i2c_Mcc"],"i2c_Mcc", jpTbin);
      book(_h["i2c_pTcc"],"i2c_pTcc", jpTbin);
      book(_h["i2c_pTOnMcc"],"i2c_pTOnMcc",std::vector<double>{10.,20.,30.,40.,50.,60., 80., 100., 120., 140., 160., 180., 200., 240., 280., 340., 400.});
      book(_h["pTc2OnpTc1c2"],"pTc2OnpTc1c2",std::vector<double>{0,0.05,0.1,0.15,0.2,0.25,0.3,0.35,0.4,0.45,0.5});
//      book(_h["ic_nCJets"],1,15,1);

//////////////////////////////////////////////////

    }

    /// Perform the per-event analysis
    void analyze(const Event& event) {

      // Abnormally large Event weight in Sherpa2.2.1
      if (std::abs(event.weights()[0]) > 100.) vetoEvent;

      // access fiducial electrons and muons
      const Particle *l1 = nullptr, *l2 = nullptr;
      auto muons = apply<DressedLeptons>(event, "DressedMuons").dressedLeptons();
      auto elecs = apply<DressedLeptons>(event, "DressedElectrons").dressedLeptons();

      // Dilepton selection 
      if (muons.size()+elecs.size()!=2) vetoEvent;
      if      (muons.size()==2) { l1=&muons[0]; l2=&muons[1]; }
      else if (elecs.size()==2) { l1=&elecs[0]; l2=&elecs[1]; }
//      else if (elecs.size()+muons.size()==2) { l1=&elecs[0]; l2=&muons[0]; }
      else vetoEvent;

      if (_mode == 2 && muons.size() !=2) vetoEvent;
      if (_mode == 1 && elecs.size() !=2) vetoEvent;

      // Dilepton selection :: Z mass peak.
      if ( !inRange((l1->mom()+l2->mom()).mass()/GeV, 76.0, 106.0) ) vetoEvent;

      // lepton-jet overlap removal (note: muons are not included in jet finding)
      // Jets eta < 2.5, pT > 30GeV for overlap removal
	    Jets jets;
	    const Jets& alljets = apply<JetAlg>(event, "jets").jetsByPt(Cuts::pT>20*GeV && Cuts::absrap < 2.5 );
      // Use jets with dR > 0.4 of lepton
	    for (const Jet& j : alljets) {
	    	bool keep = true;
		    for (const  Particle& l : elecs) keep &= deltaR(j, l) > 0.4;
		    for (const  Particle& l : muons) keep &= deltaR(j, l) > 0.4;
		    if (keep)  jets += j;
	    }

      // Calculate the observables
      double HT = 0.;
      double jet0pT = 0.;
      double cljetpT = 0.;
      double minDR = 99.;

      // Dilepton
      auto ll = (l1->mom() + l2->mom());
      double ZpT = ll.pT();
      double Zm  = ll.mass(); 
      double Zphi = ll.phi();
      double Zy = ll.rapidity();
      double Zeta = ll.eta();

//      if (apply<MissingMomentum>(event, "MET").missingPt() > 60*GeV && ZpT < 150*GeV) vetoEvent;
      std::cout << "Event: " << event.genEvent()->event_number() << ", ZMass: " << Zm/GeV << std::endl;
      _h["ZMass"]->fill(Zm/GeV);

      // Require cjets to be above 5 GeV for ghost matching
      std::vector<Jet> cJets;
      for (auto j:jets) {
	const bool isBtagged = j.bTagged(Cuts::pT > 5*GeV);
        if (isBtagged) {
          HT += j.pT();
          cJets.push_back(j);
        }
      }
      const size_t Ncjets = cJets.size();
      // Exclusive NcJets, jet pT > 20  GeV
      std::cout << "Event: " << event.genEvent()->event_number() << ", NcJets_excl: " << Ncjets << std::endl;
      _h["NcJets_excl"]->fill(Ncjets);
      // Inclusive NcJets, jet pT > 20 GeV
      for(size_t i = 0; i <= Ncjets; ++i) { 
         std::cout << "Event: " << event.genEvent()->event_number() << ", NcJets_incl: " << i << std::endl;
        _h["NcJets_incl"]->fill(i);
      }

      // NJets >= 1      
      if (Ncjets < 1) vetoEvent;

      float Zcm = (ll+cJets[0].momentum()).mass()/GeV;
///////////////////////////////////
//      _h["ic_nCJets"]->fill(1);

      double dYZc=fabs(Zy-cJets[0].rapidity());
      double dEtaZc=fabs(Zeta-cJets[0].eta());
      double dPhiZc=acos(cos(fabs(Zphi-cJets[0].phi())));
      double dRZc  =sqrt(dEtaZc*dEtaZc+dPhiZc*dPhiZc);
      double Xf = cJets[0].pT()*sinh(cJets[0].eta())/(13000); 

      std::cout << "Event: " << event.genEvent()->event_number() << ", i1c_ZpT: " << ZpT/GeV << std::endl;
      _h["i1c_ZpT"]->fill(ZpT/GeV);
      std::cout << "Event: " << event.genEvent()->event_number() << ", i1c_ZY: " << fabs(Zy) << std::endl;
      _h["i1c_ZY"]->fill(fabs(Zy));
      std::cout << "Event: " << event.genEvent()->event_number() << ", i1c_dPhiZc: " << dPhiZc << std::endl;
      _h["i1c_dPhiZc"]->fill(dPhiZc);
      std::cout << "Event: " << event.genEvent()->event_number() << ", i1c_dRZc: " << dRZc << std::endl;
      _h["i1c_dRZc"]->fill(dRZc);
      std::cout << "Event: " << event.genEvent()->event_number() << ", i1c_dYZc: " << dYZc << std::endl;
      _h["i1c_dYZc"]->fill(dYZc);
      std::cout << "Event: " << event.genEvent()->event_number() << ", i1c_MZc: " << Zcm << std::endl;
      _h["i1c_MZc"]->fill(Zcm);

      std::cout << "Event: " << event.genEvent()->event_number() << ", i1c_cpT: " << cJets[0].pT()/GeV << std::endl;
      _h["i1c_cpT"]->fill(cJets[0].pT()/GeV);
      std::cout << "Event: " << event.genEvent()->event_number() << ", i1c_cm: " << cJets[0].mass()/GeV << std::endl;
      _h["i1c_cm"]->fill(cJets[0].mass()/GeV);

      std::cout << "Event: " << event.genEvent()->event_number() << ", i1c_cY: " << cJets[0].absrapidity() << std::endl;
      _h["i1c_cY"]->fill(cJets[0].absrapidity());
      std::cout << "Event: " << event.genEvent()->event_number() << ", i1c_Xf: " << Xf << std::endl;
      _h["i1c_Xf"]->fill(Xf);
////////////////////////////////////////////////
      if ( Ncjets >1 ) {
	
	
//     	_h["ic_nCJets"]->fill(2,weight); //inclusive 2-c
	float dYcc   = fabs(cJets[0].rapidity()-cJets[1].rapidity());
	float dPhicc = acos(cos(fabs(cJets[0].phi()-cJets[1].phi())));
	float dRcc   = deltaR(cJets[0], cJets[1]) ;
	float Mcc    = (cJets[0].momentum() + cJets[1].momentum()).mass()/GeV;
	float Ptcc   = (cJets[0].momentum() + cJets[1].momentum()).pt()/GeV;
	
   std::cout << "Event: " << event.genEvent()->event_number() << ", i2c_ZpT: " << ZpT/GeV << std::endl;
	_h["i2c_ZpT"]    ->fill(ZpT/GeV);
   std::cout << "Event: " << event.genEvent()->event_number() << ", i2c_dPhicc: " << dPhicc << std::endl;
	_h["i2c_dPhicc"] ->fill(dPhicc);
  std::cout << "Event: " << event.genEvent()->event_number() << ", i2c_dRcc: " << dRcc << std::endl;
	_h["i2c_dRcc"]   ->fill(dRcc);  
  std::cout << "Event: " << event.genEvent()->event_number() << ", i2c_dYcc: " << dYcc << std::endl;
	_h["i2c_dYcc"]   ->fill(dYcc);
  std::cout << "Event: " << event.genEvent()->event_number() << ", i2c_Mcc: " << Mcc/GeV << std::endl;
	_h["i2c_Mcc"]    ->fill(Mcc/GeV);
  std::cout << "Event: " << event.genEvent()->event_number() << ", i2c_pTcc: " << Ptcc/GeV << std::endl;
	_h["i2c_pTcc"]   ->fill(Ptcc/GeV);
 std::cout << "Event: " << event.genEvent()->event_number() << ", i2c_pTOnMcc: " << Ptcc/Mcc << std::endl;
	_h["i2c_pTOnMcc"]->fill(Ptcc/Mcc);
      }

/////////////////////////////////////////////////////////

      // Z pT
      std::cout << "Event: " << event.genEvent()->event_number() << ", ZpTjet: " << ZpT/GeV << std::endl;
      _h["ZpTjet"]->fill(ZpT/GeV);

      // HT
      HT += ZpT ;
      std::cout << "Event: " << event.genEvent()->event_number() << ", HT: " << HT/GeV << std::endl;
      _h["HT"]->fill(HT/GeV);

      //Leading jet kinematics
      jet0pT = cJets[0].pT();
      std::cout << "Event: " << event.genEvent()->event_number() << ", leadingJet_pT: " << jet0pT/GeV << std::endl;
      _h["leadingJet_pT"]->fill(jet0pT/GeV);
      
      // Z/J pT ratio
      std::cout << "Event: " << event.genEvent()->event_number() << ", ZJ_pTRatio: " << ZpT/jet0pT << std::endl;
      _h["ZJ_pTRatio"]->fill(ZpT/jet0pT);

      // Kinematics of 2 leading jets
      if (Ncjets > 1) {
      std::cout << "Event: " << event.genEvent()->event_number() << ", 2ndcJet_pT: " << cJets[1].pT()/GeV << std::endl;
        _h["2ndcJet_pT"]->fill(cJets[1].pT()/GeV);
      std::cout << "Event: " << event.genEvent()->event_number() << ", i2c_2ndcm: " << cJets[1].mass()/GeV << std::endl;
	_h["i2c_2ndcm"]->fill(cJets[1].mass()/GeV);
      std::cout << "Event: " << event.genEvent()->event_number() << ", pTc2OnpTc1c2: " << cJets[1].pT()/(cJets[0].pT()+cJets[1].pT()) << std::endl;
        _h["pTc2OnpTc1c2"]->fill(cJets[1].pT()/(cJets[0].pT()+cJets[1].pT()));

        // Dijet mass
        std::cout << "Event: " << event.genEvent()->event_number() << ", mjj: " << (cJets[0].mom()+cJets[1].mom()).mass()/GeV << std::endl;
        _h["mjj"]->fill((cJets[0].mom()+cJets[1].mom()).mass()/GeV);
        // Dijet delta Phi
        std::cout << "Event: " << event.genEvent()->event_number() << ", jjDPhi: " << deltaPhi(cJets[0].mom(), cJets[1].mom())/GeV << std::endl;
        _h["jjDPhi"]->fill(deltaPhi(cJets[0].mom(), cJets[1].mom())/GeV);
      }

      // closest jet to Z
      for (auto j : cJets) {
        if (deltaR( j.mom(), ll ,RAPIDITY) < minDR) {
          minDR = deltaR( j.mom(), ll ,RAPIDITY);
          cljetpT = j.pT();
        }
      }
     std::cout << "Event: " << event.genEvent()->event_number() << ", closestJet_pT: " << cljetpT/GeV << std::endl;
      _h["closestJet_pT"]->fill(cljetpT/GeV);
      std::cout << "Event: " << event.genEvent()->event_number() << ", DR: " << minDR << std::endl;
      _h["DR"]->fill(minDR);
      

      // Phase space with DR<2.0       
      if (minDR < 2.0) {
        if (Ncjets > 1) {  
          std::cout << "Event: " << event.genEvent()->event_number() << ", mjj_loDR: " << (cJets[0].mom()+cJets[1].mom()).mass()/GeV << std::endl;
          _h["mjj_loDR"]->fill((cJets[0].mom()+cJets[1].mom()).mass()/GeV);
          std::cout << "Event: " << event.genEvent()->event_number() << ", jjDPhi_loDR: " << deltaPhi(cJets[0].mom(), cJets[1].mom())/GeV << std::endl;
          _h["jjDPhi_loDR"]->fill(deltaPhi(cJets[0].mom(), cJets[1].mom())/GeV);
        }
       std::cout << "Event: " << event.genEvent()->event_number() << ", NcJets_loDR: " << Ncjets << std::endl;
        _h["NcJets_loDR"]->fill(Ncjets);
        std::cout << "Event: " << event.genEvent()->event_number() << ", ZJ_pTRatio_loDR: " << ZpT/jet0pT << std::endl;
        _h["ZJ_pTRatio_loDR"]->fill(ZpT/jet0pT);
      } else {
        if (Ncjets > 1) {  
          std::cout << "Event: " << event.genEvent()->event_number() << ", mjj_hiDR: " << (cJets[0].mom()+cJets[1].mom()).mass()/GeV << std::endl;
          _h["mjj_hiDR"]->fill((cJets[0].mom()+cJets[1].mom()).mass()/GeV);
         std::cout << "Event: " << event.genEvent()->event_number() << ", jjDPhi_hiDR: " << deltaPhi(cJets[0].mom(), cJets[1].mom())/GeV << std::endl;
          _h["jjDPhi_hiDR"]->fill(deltaPhi(cJets[0].mom(), cJets[1].mom())/GeV);
        }
        std::cout << "Event: " << event.genEvent()->event_number() << ", NcJets_hiDR: " << Ncjets << std::endl;
        _h["NcJets_hiDR"]->fill(Ncjets);
        std::cout << "Event: " << event.genEvent()->event_number() << ", ZJ_pTRatio_hiDR: " << ZpT/jet0pT << std::endl;
        _h["ZJ_pTRatio_hiDR"]->fill(ZpT/jet0pT);
      }

    }

    void finalize() {
      double xsec = crossSection()/picobarn/sumW();
      if (_mode == 0) xsec = xsec * 0.5;

      // Normalize to cross section.
      scale(_h["ZMass"],xsec);
      scale(_h["NJets_incl"],xsec);
      scale(_h["NJets_excl"],xsec);
      scale(_h["NJets_loDR"],xsec);
      scale(_h["NJets_hiDR"],xsec);
      scale(_h["leadingJet_pT"],xsec);
      scale(_h["closestJet_pT"],xsec);
      scale(_h["2ndJet_pT"],xsec);
      scale(_h["ZJ_pTRatio"],xsec);
      scale(_h["ZJ_pTRatio_loDR"],xsec);
      scale(_h["ZJ_pTRatio_hiDR"],xsec);
      scale(_h["DR"],xsec);
      scale(_h["HT"],xsec);
      scale(_h["mjj"],xsec);
      scale(_h["mjj_loDR"],xsec);
      scale(_h["mjj_hiDR"],xsec);
      scale(_h["jjDPhi"],xsec);
      scale(_h["jjDPhi_loDR"],xsec);
      scale(_h["jjDPhi_hiDR"],xsec);
      scale(_h["ZpTjet"],xsec);
////////////////////////////////////////////////////////////////////
      
      scale(_h["i1c_ZpT"],xsec);
      scale(_h["i1c_ZY"],xsec);
      scale(_h["i1c_dPhiZc"],xsec);
      scale(_h["i1c_dRZc"],xsec);
      scale(_h["i1c_dYZc"],xsec);
      scale(_h["i1c_MZc"],xsec);

      scale(_h["i1c_cpT"],xsec);
      scale(_h["i1c_cm"],xsec);
      scale(_h["i1c_cY"],xsec);
      scale(_h["i1c_Xf"],xsec);

      scale(_h["i2c_ZpT"],xsec);
      scale(_h["i2c_dPhicc"],xsec);
      scale(_h["i2c_dRcc"],xsec);
      scale(_h["i2c_dYcc"],xsec);
      scale(_h["i2c_Mcc"],xsec);
      scale(_h["i2c_pTcc"],xsec);
      scale(_h["i2c_pTOnMcc"],xsec);
      scale(_h["pTc2OnpTc1c2"],xsec);

      scale(_h["2ndcJet_pT"],xsec);
      scale(_h["i2c_2ndcm"],xsec);

//      scale(_h["ic_nCJets"],xsec);
///////////////////////////////////////////////////////////////////
    } // end of finalize

    //@}

    // define histograms
    size_t _mode;
    map<string, Histo1DPtr> _h;
 
  };

  DECLARE_RIVET_PLUGIN(Zcc_rivet);
}
