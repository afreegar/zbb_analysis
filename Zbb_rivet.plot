BEGIN PLOT /Zbb_rivet/.*
XTwosidedTicks=1
YTwosidedticks=1
LeftMargin=1.5
XMinorTickMarks=0
LogY=1
LegendAlign=r
Title=$Z \rightarrow \ell^+ \ell^-$
END PLOT

BEGIN PLOT /Zbb_rivet/2ndbJet_pT
XLabel=$p_{T}(b2)$ [GeV]
YLabel=Events
END PLOT

BEGIN PLOT /Zbb_rivet/DR
XLabel=minDR(Z,J)
YLabel=Events
END PLOT

BEGIN PLOT /Zbb_rivet/HT
XLabel=$H_{T}$ [GeV]
YLabel=Events
END PLOT

BEGIN PLOT /Zbb_rivet/NbJets_excl
XLabel=$N_\text{bjets}$ exclusive
YLabel=Events
END PLOT

BEGIN PLOT /Zbb_rivet/NbJets_hiDR
XLabel=$N_\text{bjets}$ minDR(Z,J)$\ge\text{2.0}$
YLabel=Events
END PLOT

BEGIN PLOT /Zbb_rivet/NbJets_incl
XLabel=$N_\text{bjets}$ inclusive
YLabel=Events
END PLOT

BEGIN PLOT /Zbb_rivet/NbJets_loDR
XLabel=$N_\text{jets}$ minDR(Z,J)$\le\text{2.0}$
YLabel=Events
END PLOT

BEGIN PLOT /Zbb_rivet/ZJpT
XLabel=$p_{T}(ZJ)$ [GeV]
YLabel=Events
END PLOT

BEGIN PLOT /Zbb_rivet/ZJpT_hiDR
XLabel=$p_{T}(ZJ)$ [GeV]
YLabel=Events
END PLOT

BEGIN PLOT /Zbb_rivet/ZJpT_loDR
XLabel=$p_{T}(ZJ)$ [GeV]
YLabel=Events
END PLOT

BEGIN PLOT /Zbb_rivet/ZMass
XLabel=$M(Z)$ [GeV]
YLabel=Events
END PLOT

BEGIN PLOT /Zbb_rivet/ZpTjet
XLabel=$p_{T}(Z)$ [GeV]
YLabel=Events
END PLOT

BEGIN PLOT /Zbb_rivet/clJetpT
XLabel=$p_{T}$(closest jet) [GeV]
YLabel=Events
END PLOT

BEGIN PLOT /Zbb_rivet/dPhijj
XLabel=$d\phi(j1,j2)$
YLabel=Events
END PLOT

BEGIN PLOT /Zbb_rivet/dPhijj_hiDR
XLabel=$d\phi(j1,j2)$ highDR
YLabel=Events
END PLOT

BEGIN PLOT /Zbb_rivet/dPhijj_loDR
XLabel=$d\phi(j1,j2)$ lowDR
YLabel=Events
END PLOT

BEGIN PLOT /Zbb_rivet/i1b_2ndbm
XLabel=$p_{T}(b2)$ [GeV]
YLabel=Events
END PLOT

BEGIN PLOT /Zbb_rivet/i1b_MZb
XLabel=$M(Z,b)$ [GeV]
YLabel=Events
END PLOT

BEGIN PLOT /Zbb_rivet/i1b_ZY
XLabel=$y(Z)$
YLabel=Events
END PLOT

BEGIN PLOT /Zbb_rivet/i1b_ZpT
XLabel=$p_{T}(Z)$ [GeV]
YLabel=Events
END PLOT

BEGIN PLOT /Zbb_rivet/i1b_bY
XLabel=$y(b1)$ [GeV]
YLabel=Events
END PLOT

BEGIN PLOT /Zbb_rivet/i1b_bm
XLabel=$M(b1)$ [GeV]
YLabel=Events
END PLOT

BEGIN PLOT /Zbb_rivet/i1b_bpT
XLabel=$p_{T}(b1)$ [GeV]
YLabel=Events
END PLOT

BEGIN PLOT /Zbb_rivet/i1b_dPhiZb
XLabel=$d\phi(Z,b)$
YLabel=Events
END PLOT

BEGIN PLOT /Zbb_rivet/i1b_dRZb
XLabel=$dR(Z,b)$
YLabel=Events
END PLOT

BEGIN PLOT /Zbb_rivet/i1b_dYZb
XLabel=$dy(Z,b)$
YLabel=Events
END PLOT

BEGIN PLOT /Zbb_rivet/i1b_Xf  
XLabel=$X_F(b)$
YLabel=Events
END PLOT

BEGIN PLOT /Zbb_rivet/i2b_Mbb
XLabel=$M(b1,b2)$ [GeV]
YLabel=Events
END PLOT

BEGIN PLOT /Zbb_rivet/i2b_ZpT
XLabel=$p_{T}(Z)$ [GeV]
YLabel=Events
END PLOT

BEGIN PLOT /Zbb_rivet/i2b_dPhibb
XLabel=$d\phi(b1,b2)$
YLabel=Events
END PLOT

BEGIN PLOT /Zbb_rivet/i2b_dRbb
XLabel=$dR(b1,b2)$
YLabel=Events
END PLOT

BEGIN PLOT /Zbb_rivet/i2b_dYbb
XLabel=$dY(b1,b2)$
YLabel=Events
END PLOT

BEGIN PLOT /Zbb_rivet/i2b_pTOnMbb
XLabel=$p_{T}(b2)/M(b1,b2)$ [GeV]
YLabel=Events
END PLOT

BEGIN PLOT /Zbb_rivet/i2b_pTbb
XLabel=$p_{T}(b1,b2)$ [GeV]
YLabel=Events
END PLOT

BEGIN PLOT /Zbb_rivet/j0pT
XLabel=$p_{T}(j1)$ [GeV]
YLabel=Events
END PLOT

BEGIN PLOT /Zbb_rivet/j1pT
XLabel=$p_{T}(j2)$ [GeV]
YLabel=Events
END PLOT
BEGIN PLOT /Zbb_rivet/mjj
XLabel=$M(j1,j2)$ [GeV]
YLabel=Events
END PLOT

BEGIN PLOT /Zbb_rivet/mjj_hiDR
XLabel=$M(j1,j2)$ highDR [GeV]
YLabel=Events
END PLOT

BEGIN PLOT /Zbb_rivet/mjj_loDR
XLabel=$M(j1,j2)$ lowDR [GeV]
YLabel=Events
END PLOT

BEGIN PLOT /Zbb_rivet/pTb2OnpTb1b2
XLabel=$p_{T}(b2)/p_{T}(b1,b2)$
YLabel=Events
END PLOT
