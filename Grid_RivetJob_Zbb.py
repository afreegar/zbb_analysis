theApp.EvtMax = -1

import AthenaPoolCnvSvc.ReadAthenaPool

from AthenaCommon.AlgSequence import AlgSequence
job = AlgSequence()

from Rivet_i.Rivet_iConf import Rivet_i
rivet = Rivet_i()
import os
rivet.AnalysisPath = os.environ['PWD']

rivet.Analyses += [ 'Zbb_rivet' ]
rivet.RunName = ''
rivet.HistoFile = 'Zbb_truth'
rivet.SkipWeights=True
#rivet.CrossSection = 1.0
#rivet.IgnoreBeamCheck = True
job += rivet
