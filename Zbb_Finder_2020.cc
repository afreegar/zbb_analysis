// -*- C++ -*-
#include "Rivet/Analysis.hh"
#include "Rivet/Projections/ZFinder.hh"
#include "Rivet/Projections/FastJets.hh"
#include "Rivet/Projections/VetoedFinalState.hh"
#include "Rivet/Projections/HeavyHadrons.hh"
#include "Rivet/Projections/PromptFinalState.hh"
#include <iostream>

namespace Rivet {

  /// Z+b(b) in pp at 13 TeV
  class Zbb_Finder_2020 : public Analysis {
  public:

    /// Constructor
    DEFAULT_RIVET_ANALYSIS_CTOR(Zbb_Finder_2020);

    /// Book histograms and initialise projections before the run
    void init() {

	    const FinalState fs;
	    // Define fiducial cuts for the leptons in the ZFinder
	    Cut lepcuts = (Cuts::pT > 28*GeV) & (Cuts::abseta < 2.5);
	    ZFinder zfinderE(fs, lepcuts, PID::ELECTRON, 76*GeV, 106*GeV);
	    ZFinder zfinderM(fs, lepcuts, PID::MUON, 76*GeV, 106*GeV);
	    declare(zfinderE, "zfinderE");
            declare(zfinderM, "zfinderM");

	    declare(HeavyHadrons(), "HFHadrons");

	    // // Photons
	    FinalState photons(Cuts::abspid == PID::PHOTON);
	    // Muons
	    PromptFinalState bare_mu(Cuts::abspid == PID::MUON, true); 
	    DressedLeptons all_dressed_mu(photons, bare_mu, 0.1, Cuts::abseta < 2.5, true);
	    // Electrons
	    PromptFinalState bare_el(Cuts::abspid == PID::ELECTRON, true); 
	    DressedLeptons all_dressed_el(photons, bare_el, 0.1, Cuts::abseta < 2.5, true);

	    //Jet forming
	    VetoedFinalState vfs(FinalState(Cuts::abseta < 4.5));
	    vfs.addVetoOnThisFinalState(all_dressed_el);
	    vfs.addVetoOnThisFinalState(all_dressed_mu);
	   
	    FastJets jets(vfs, FastJets::ANTIKT, 0.4, JetAlg::Muons::ALL, JetAlg::Invisibles::DECAY);
	    declare(jets, "jets");
      
	    // book histos - binning taken from twiki
        
        std::vector<double> ZpTbin = {0., 15., 30., 40., 50., 60., 75., 90., 110., 130., 180., 240., 350., 500., 1000.};
        std::vector<double> Zmassbin = {70.,72.,74.,76.,78.,80.,82.,84.,86.,88.,90.,92.,94.,96.,98.,100.,102.,104.,106.,108.,110.};
        std::vector<double> Zabsybin = {0.,0.25,0.5,0.75,1.,1.25,1.5,2.,2.25,2.5};

        std::vector<double> deltaRbin = {0, 0.4, 0.9, 1.3, 1.8, 2.3, 2.7, 3.1, 3.5, 4.0, 5.0};
        std::vector<double> deltaYbin = {0, 0.4, 0.9, 1.3, 1.8, 2.3, 2.7, 3.1, 3.5, 4.0, 5.0};
        std::vector<double> deltaPhibin = {0,0.2625,0.525,0.7875,1.05,1.3125,1.575,1.8375,2.1,2.3625,2.625,2.8875,3.15};
        std::vector<double> jpTbin = {17., 20., 24., 30., 36., 43., 60., 80., 100., 130., 170., 230., 330., 500., 900.};


        book(_h["i1b_ZpT"], "i1b_ZpT", ZpTbin);
        book(_h["i1b_ZY"],"i1b_ZY", Zabsybin);
        book(_h["i1b_dPhiZb"],"i1b_dPhiZb",deltaPhibin);
        book(_h["i1b_dRZb"],"i1b_dRZb",deltaRbin);
        book(_h["i1b_dYZb"],"i1b_dYZb",deltaYbin);
        book(_h["i1b_MZb"],"i1b_MZb", Zmassbin);
        book(_h["i1b_bpT"],"i1b_bpT",jpTbin);
        book(_h["i1b_bm"],"i1b_bm", jpTbin);
        book(_h["i1b_bY"],"i1b_bY",Zabsybin);
        book(_h["i1b_Xf"],"i1b_Xf", std::vector<double>{0.,0.01,0.02,0.03,0.04,0.05,0.06,0.07,0.08,0.09,0.10,0.11,0.12,0.13,0.14,0.15,0.16,0.17,0.18,0.19,0.20,0.21,0.22,0.23,0.24,0.25,0.26,0.27,0.28,0.29,0.30,0.31,0.32,0.33,0.34,0.35,0.36});


      
        book(_h["i2b_ZpT"],"i2b_ZpT",ZpTbin);
        book(_h["i2b_dPhibb"],"i2b_dPhibb",deltaPhibin);
        book(_h["i2b_dRbb"],"i2b_dRbb",deltaRbin);
        book(_h["i2b_dYbb"],"i2b_dYbb",deltaYbin);
        book(_h["i2b_Mbb"],"i2b_Mbb", jpTbin);
        book(_h["i2b_pTbb"],"i2b_pTbb", jpTbin);
        book(_h["i2b_pTOnMbb"],"i2b_pTOnMbb", std::vector<double>{10.,20.,30.,40.,50.,60., 80., 100., 120., 140., 160., 180., 200., 240., 280., 340., 400.});
        book(_h["pTb2OnpTb1b2"],"pTb2OnpTb1b2",std::vector<double>{0,0.05,0.1,0.15,0.2,0.25,0.3,0.35,0.4,0.45,0.5});

        book(_h["ib_nBJets"],"ib_nBJets",std::vector<double>{0,1,2});                                      
	} 

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	/// Perform the per-event analysis
	void analyze(const Event& event) {

      // Abnormally large Event weight in Sherpa2.2.1
      if (std::abs(event.weights()[0]) > 100.) vetoEvent;
	  
	  const ZFinder& zfinderE = apply<ZFinder>(event, "zfinderE");
	  const Particles& els = zfinderE.constituents();
	    const ZFinder& zfinderM = apply<ZFinder>(event, "zfinderM");
	    const Particles& mus = zfinderM.constituents();
	    if ( !(els.size()==2 || mus.size()==2 ) )  vetoEvent;
	    float Vpt =0;
	    float Vy=0;
	    float Veta=0;
	    float Vphi=0;
	    float Zbm=0;

	    if ( els.size()==2 ) {
		Vpt =zfinderE.boson().pt()/GeV;
		Vphi=zfinderE.boson().phi();
		Vy=zfinderE.boson().rapidity();
		Veta=zfinderE.boson().eta();
	    } else {
		Vpt=zfinderM.boson().pt()/GeV;
		Vphi=zfinderM.boson().phi();
		Vy=zfinderM.boson().rapidity();
		Veta=zfinderM.boson().eta();
	    }
      
	    Jets jets;
	    const Jets& alljets = apply<JetAlg>(event, "jets").jetsByPt(Cuts::pT>20*GeV && Cuts::absrap < 2.5 );
	    for (const Jet& j : alljets) {
	    	bool keep = true;
		    for (const  Particle& l : els) keep &= deltaR(j, l) > 0.4;
		    for (const  Particle& l : mus) keep &= deltaR(j, l) > 0.4;
		    if (keep)  jets += j;
	    }

	    Jets btagged;
	    Particles allBs = apply<HeavyHadrons>(event, "HFHadrons").bHadrons(5.0*GeV);
	    Particles matchedBs;

	    for (const Jet& j : jets) {
		Jet closest_j;
		Particle closest_b;
		double minDR_j_b = 10;

		for (const  Particle& bHad : allBs) {
		    bool alreadyMatched = false;
	      
		    for (const Particle& bMatched : matchedBs) {
			if(bMatched.isSame(bHad)) alreadyMatched = true;
		    }
		    if(alreadyMatched) { 
			continue;
			alreadyMatched= false; 
		    }
	      
		    double DR_j_b = deltaR(j, bHad);
		    if ( DR_j_b <= 0.3 && DR_j_b < minDR_j_b) {
			minDR_j_b = DR_j_b;
			closest_j = j;
			closest_b = bHad;
		    }
		  }
	  
		if( minDR_j_b < 0.3) {
		    btagged += closest_j;
		    matchedBs += closest_b;
		  }
	    }
	    //size_t njets = jets.size();
	    size_t ntags = btagged.size();
	    if ( ntags<1 ) vetoEvent;
  
            std::cout << "Event: " << event.genEvent()->event_number() << ", nBJets: " << 1 << std::endl;
	    _h["ib_nBJets"]->fill(1); //inclusive 1-b
      
	    if ( els.size()==2 ) {
	    Zbm = (zfinderE.boson().momentum()+btagged[0].momentum()).mass()/GeV;
	    }
	    if ( mus.size()==2 ) {
	    Zbm = (zfinderM.boson().momentum()+btagged[0].momentum()).mass()/GeV;
	    }

	    double dYVb=fabs(Vy-btagged[0].rapidity());
	    double dEtaVb=fabs(Veta-btagged[0].eta());
	    double dPhiVb=acos(cos(fabs(Vphi-btagged[0].phi())));
	    double dRVb  =sqrt(dEtaVb*dEtaVb+dPhiVb*dPhiVb);
            double Xf = btagged[0].pT()*sinh(btagged[0].eta())/(13000); 

       std::cout << "Event: " << event.genEvent()->event_number() << ", i1b_ZpT: " << Vpt/GeV << std::endl;
       _h["i1b_ZpT"]   ->fill(Vpt/GeV);
            std::cout << "Event: " << event.genEvent()->event_number() << ", i1b_ZY: " << fabs(Vy) << std::endl;
       _h["i1b_ZY"]    ->fill(fabs(Vy));
            std::cout << "Event: " << event.genEvent()->event_number() << ", i1b_dPhiZb: " << dPhiVb << std::endl;
       _h["i1b_dPhiZb"]->fill(dPhiVb);
            std::cout << "Event: " << event.genEvent()->event_number() << ", i1b_dRZb: " << dRVb << std::endl;
       _h["i1b_dRZb"]  ->fill(dRVb);
            std::cout << "Event: " << event.genEvent()->event_number() << ", i1b_dYZb: " << dYVb << std::endl;
       _h["i1b_dYZb"]  ->fill(dYVb);
            std::cout << "Event: " << event.genEvent()->event_number() << ", i1b_bpT: " << btagged[0].pt()/GeV << std::endl;
       _h["i1b_bpT"]   ->fill(btagged[0].pt()/GeV);
            std::cout << "Event: " << event.genEvent()->event_number() << ", i1b_bm: " << btagged[0].mass()/GeV << std::endl;
       _h["i1b_bm"]    ->fill(btagged[0].mass()/GeV);

       std::cout << "Event: " << event.genEvent()->event_number() << ", i1b_bY: " << btagged[0].absrapidity() << std::endl;
       _h["i1b_bY"]    ->fill(btagged[0].absrapidity()); 
       std::cout << "Event: " << event.genEvent()->event_number() << ", i1b_Xf: " << Xf << std::endl;
       _h["i1b_Xf"]->fill(Xf);
       std::cout << "Event: " << event.genEvent()->event_number() << ", i1b_MZb: " << Zbm << std::endl;
       _h["i1b_MZb"]->fill(Zbm);


	   if ( ntags>1 ) {
                std::cout << "Event: " << event.genEvent()->event_number() << ", ib_nBJets: " << 2 << std::endl;
	 	_h["ib_nBJets"]->fill(2); //inclusive 2-b
		
	    float dYbb   = fabs(btagged[0].rapidity()-btagged[1].rapidity());
	    float dPhibb = acos(cos(fabs(btagged[0].phi()-btagged[1].phi())));
	    float dRbb   = deltaR(btagged[0], btagged[1]) ;
	    float Mbb    = (btagged[0].momentum() + btagged[1].momentum()).mass()/GeV;
	    float Ptbb   = (btagged[0].momentum() + btagged[1].momentum()).pt()/GeV;
	  
               std::cout << "Event: " << event.genEvent()->event_number() << ", i2b_ZpT: " << Vpt/GeV << std::endl;
	    _h["i2b_ZpT"]    ->fill(Vpt/GeV);
               std::cout << "Event: " << event.genEvent()->event_number() << ", i2b_dPhibb: " << dPhibb << std::endl;
            _h["i2b_dPhibb"] ->fill(dPhibb);
               std::cout << "Event: " << event.genEvent()->event_number() << ", i2b_dRbb: " << dRbb << std::endl;
	    _h["i2b_dRbb"]   ->fill(dRbb);
               std::cout << "Event: " << event.genEvent()->event_number() << ", i2b_dYbb: " << dYbb << std::endl;
            _h["i2b_dYbb"]   ->fill(dYbb);
               std::cout << "Event: " << event.genEvent()->event_number() << ", i2b_Mbb: " << Mbb/GeV << std::endl;
	    _h["i2b_Mbb"]    ->fill(Mbb/GeV);
               std::cout << "Event: " << event.genEvent()->event_number() << ", i2b_pTbb: " << Ptbb/GeV << std::endl;
	    _h["i2b_pTbb"]   ->fill(Ptbb/GeV);
               std::cout << "Event: " << event.genEvent()->event_number() << ", i2b_pTOnMbb: " << Ptbb/Mbb << std::endl;
	    _h["i2b_pTOnMbb"]->fill(Ptbb/Mbb);
               std::cout << "Event: " << event.genEvent()->event_number() << ", pTb2OnpTb1b2: " << btagged[1].pT()/(btagged[0].pT()+btagged[1].pT()) << std::endl;
            _h["pTb2OnpTb1b2"]->fill(btagged[1].pT()/(btagged[0].pT()+btagged[1].pT()));

	  }
	}
    

	void finalize() {      
	    for (map<string, Histo1DPtr>::iterator hit = _h.begin(); hit != _h.end(); ++hit) {
		scale(hit->second, crossSectionPerEvent());
	    }      
	}
      
    protected:
	size_t _mode;
      
    private:
	map<string, Histo1DPtr> _h;
      
    };
    

    DECLARE_RIVET_PLUGIN(Zbb_Finder_2020);
}