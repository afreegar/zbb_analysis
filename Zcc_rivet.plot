BEGIN PLOT /Zcc_rivet/.*
XTwosidedTicks=1
YTwosidedticks=1
LeftMargin=1.5
XMinorTickMarks=0
LogY=1
LegendAlign=r
Title=$Z \rightarrow \ell^+ \ell^-$
END PLOT

BEGIN PLOT /Zcc_rivet/2ndcJet_pT
XLabel=$p_{T}(c2)$ [GeV]
YLabel=Events
END PLOT

BEGIN PLOT /Zcc_rivet/DR
XLabel=minDR(Z,J)
YLabel=Events
END PLOT

BEGIN PLOT /Zcc_rivet/HT
XLabel=$H_{T}$ [GeV]
YLabel=Events
END PLOT

BEGIN PLOT /Zcc_rivet/NcJets_excl
XLabel=$N_\text{cjets}$ exclusive
YLabel=Events
END PLOT

BEGIN PLOT /Zcc_rivet/NcJets_hiDR
XLabel=$N_\text{cjets}$ minDR(Z,J)$\ge\text{2.0}$
YLabel=Events
END PLOT

BEGIN PLOT /Zcc_rivet/NcJets_incl
XLabel=$N_\text{cjets}$ inclusive
YLabel=Events
END PLOT

BEGIN PLOT /Zcc_rivet/NcJets_loDR
XLabel=$N_\text{jets}$ minDR(Z,J)$\le\text{2.0}$
YLabel=Events
END PLOT

BEGIN PLOT /Zcc_rivet/ZJpT
XLabel=$p_{T}(ZJ)$ [GeV]
YLabel=Events
END PLOT

BEGIN PLOT /Zcc_rivet/ZJpT_hiDR
XLabel=$p_{T}(ZJ)$ [GeV]
YLabel=Events
END PLOT

BEGIN PLOT /Zcc_rivet/ZJpT_loDR
XLabel=$p_{T}(ZJ)$ [GeV]
YLabel=Events
END PLOT

BEGIN PLOT /Zcc_rivet/ZMass
XLabel=$M(Z)$ [GeV]
YLabel=Events
END PLOT

BEGIN PLOT /Zcc_rivet/ZpTjet
XLabel=$p_{T}(Z)$ [GeV]
YLabel=Events
END PLOT

BEGIN PLOT /Zcc_rivet/clJetpT
XLabel=$p_{T}$(closest jet) [GeV]
YLabel=Events
END PLOT

BEGIN PLOT /Zcc_rivet/dPhijj
XLabel=$d\phi(j1,j2)$
YLabel=Events
END PLOT

BEGIN PLOT /Zcc_rivet/dPhijj_hiDR
XLabel=$d\phi(j1,j2)$ highDR
YLabel=Events
END PLOT

BEGIN PLOT /Zcc_rivet/dPhijj_loDR
XLabel=$d\phi(j1,j2)$ lowDR
YLabel=Events
END PLOT

BEGIN PLOT /Zcc_rivet/i1c_2ndcm
XLabel=$p_{T}(c2)$ [GeV]
YLabel=Events
END PLOT

BEGIN PLOT /Zcc_rivet/i1c_MZc
XLabel=$M(Z,c)$ [GeV]
YLabel=Events
END PLOT

BEGIN PLOT /Zcc_rivet/i1c_ZY
XLabel=$y(Z)$
YLabel=Events
END PLOT

BEGIN PLOT /Zcc_rivet/i1c_ZpT
XLabel=$p_{T}(Z)$ [GeV]
YLabel=Events
END PLOT

BEGIN PLOT /Zcc_rivet/i1c_cY
XLabel=$y(c1)$ [GeV]
YLabel=Events
END PLOT

BEGIN PLOT /Zcc_rivet/i1c_cm
XLabel=$M(c1)$ [GeV]
YLabel=Events
END PLOT

BEGIN PLOT /Zcc_rivet/i1c_cpT
XLabel=$p_{T}(c1)$ [GeV]
YLabel=Events
END PLOT

BEGIN PLOT /Zcc_rivet/i1c_dPhiZc
XLabel=$d\phi(Z,c)$
YLabel=Events
END PLOT

BEGIN PLOT /Zcc_rivet/i1c_dRZc
XLabel=$dR(Z,c)$
YLabel=Events
END PLOT

BEGIN PLOT /Zcc_rivet/i1c_dYZc
XLabel=$dy(Z,c)$
YLabel=Events
END PLOT

BEGIN PLOT /Zcc_rivet/i1c_Xf  
XLabel=$X_F(c)$
YLabel=Events
END PLOT

BEGIN PLOT /Zcc_rivet/i2c_Mcc
XLabel=$M(c1,c2)$ [GeV]
YLabel=Events
END PLOT

BEGIN PLOT /Zcc_rivet/i2c_ZpT
XLabel=$p_{T}(Z)$ [GeV]
YLabel=Events
END PLOT

BEGIN PLOT /Zcc_rivet/i2c_dPhicc
XLabel=$d\phi(c1,c2)$
YLabel=Events
END PLOT

BEGIN PLOT /Zcc_rivet/i2c_dRcc
XLabel=$dR(c1,c2)$
YLabel=Events
END PLOT

BEGIN PLOT /Zcc_rivet/i2c_dYcc
XLabel=$dY(c1,c2)$
YLabel=Events
END PLOT

BEGIN PLOT /Zcc_rivet/i2c_pTOnMcc
XLabel=$p_{T}(c2)/M(c1,c2)$ [GeV]
YLabel=Events
END PLOT

BEGIN PLOT /Zcc_rivet/i2c_pTcc
XLabel=$p_{T}(c1,c2)$ [GeV]
YLabel=Events
END PLOT

BEGIN PLOT /Zcc_rivet/j0pT
XLabel=$p_{T}(j1)$ [GeV]
YLabel=Events
END PLOT

BEGIN PLOT /Zcc_rivet/j1pT
XLabel=$p_{T}(j2)$ [GeV]
YLabel=Events
END PLOT
BEGIN PLOT /Zcc_rivet/mjj
XLabel=$M(j1,j2)$ [GeV]
YLabel=Events
END PLOT

BEGIN PLOT /Zcc_rivet/mjj_hiDR
XLabel=$M(j1,j2)$ highDR [GeV]
YLabel=Events
END PLOT

BEGIN PLOT /Zcc_rivet/mjj_loDR
XLabel=$M(j1,j2)$ lowDR [GeV]
YLabel=Events
END PLOT

BEGIN PLOT /Zcc_rivet/pTc2OnpTc1c2
XLabel=$p_{T}(c2)/p_{T}(c1,c2)$
YLabel=Events
END PLOT
