1. asetup 21.6.21,AthGeneration

2. source setupRivet.sh

3. rivet-build Rivet_Zbb_2020_rivet.so Zbb_2020_rivet.cc to build it, 

4. use analysis "Zbb_rivet" in joboptions

5. Example: 
pathena Grid_RivetJob_Zbb.py --extOutFile=Zbb_truth.yoda --inDS=mc15_13TeV.950059.MGPy8EG_A14NNPDF23LO_Zmumu3jets_FxFx_valid.evgen.EVNT.e8102 --outDS=user.afreegar.ZbbRivetTestFxFx.950059.v1 --extFile=Rivet_Zbb_2020_rivet.so
