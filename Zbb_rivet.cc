// -*- C++ -*-
#include "Rivet/Analysis.hh"
#include "Rivet/Projections/FastJets.hh"
#include "Rivet/Projections/FinalState.hh"
#include "Rivet/Projections/IdentifiedFinalState.hh"
#include "Rivet/Projections/VetoedFinalState.hh"
#include "Rivet/Projections/PromptFinalState.hh"
#include "Rivet/Projections/DressedLeptons.hh"

namespace Rivet {

  /// Zbb in pp at 13 TeV
  class Zbb_rivet : public Analysis {
  public:

    /// Constructor
    DEFAULT_RIVET_ANALYSIS_CTOR(Zbb_rivet);

    /// Book histograms and initialise projections before the run
    void init() {
	
	      // Get options
      _mode = 0;
      if ( getOption("LMODE") == "ELEL") _mode = 1;
      if ( getOption("LMODE") == "MUMU") _mode = 2;
	
      // Define final state |eta| < 5
      const FinalState fs;

      IdentifiedFinalState photon_fs(fs);
      photon_fs.acceptIdPair(PID::PHOTON);
      PromptFinalState photons(photon_fs);

      IdentifiedFinalState el_id(fs);
      el_id.acceptIdPair(PID::ELECTRON);
      PromptFinalState electrons(el_id);

      IdentifiedFinalState mu_id(fs);
      mu_id.acceptIdPair(PID::MUON);
      PromptFinalState muons(mu_id);

      // Kinematic cuts for leptons
      Cut cuts_el = (Cuts::pT > 28*GeV) && (Cuts::abseta < 2.5);
      Cut cuts_mu = (Cuts::pT > 28*GeV) && (Cuts::abseta < 2.5);

      DressedLeptons dressed_electrons(photons, electrons, 0.1, cuts_el);
      declare(dressed_electrons, "DressedElectrons");

      DressedLeptons dressed_muons(photons, muons, 0.1, cuts_mu);
      declare(dressed_muons, "DressedMuons");

      FastJets jets(fs, FastJets::ANTIKT, 0.4, JetAlg::Muons::ALL, JetAlg::Invisibles::DECAY);
      declare(jets, "Jets");
      
//      declare(MissingMomentum(fs), "MET");

      std::vector<double> ZpTbin = {0., 15., 30., 40., 50., 60., 75., 90., 110., 130., 180., 240., 350., 500., 1000.};
      std::vector<double> Zmassbin = {70.,72.,74.,76.,78.,80.,82.,84.,86.,88.,90.,92.,94.,96.,98.,100.,102.,104.,106.,108.,110.};
      std::vector<double> Zabsybin = {0.,0.25,0.5,0.75,1.,1.25,1.5,1.75,2.,2.25,2.5};

      std::vector<double> deltaRbin = {0, 0.4, 0.9, 1.3, 1.8, 2.3, 2.7, 3.1, 3.5, 4.0, 5.0};
      std::vector<double> deltaYbin = {0, 0.4, 0.9, 1.3, 1.8, 2.3, 2.7, 3.1, 3.5, 4.0, 5.0};
      std::vector<double> deltaPhibin = {0,0.2625,0.525,0.7875,1.05,1.3125,1.575,1.8375,2.1,2.3625,2.625,2.8875,3.15};
      std::vector<double> jpTbin = {17., 20., 24., 30., 36., 43., 60., 80., 100., 130., 170., 230., 330., 500., 750., 1200.};

	  std::vector<double> xFbins = {0.,0.01,0.02,0.03,0.04,0.05,0.06,0.08,0.10,0.14,0.18,0.22,0.30,0.5,1};

      book(_h["ZMass"],"ZMass" , Zmassbin);

      book(_h["NbJets_incl"],"NbJets_incl" , 8, 1,9);
      book(_h["NbJets_excl"],"NbJets_excl" , 8, 1,9);
      book(_h["NbJets_loDR"],"NbJets_loDR" , 8, 1,9);
      book(_h["NbJets_hiDR"],"NbJets_hiDR" , 8, 1,9);

      book(_h["leadingJet_pT"], "j0pT",    jpTbin);
      book(_h["closestJet_pT"], "clJetpT", jpTbin);
      book(_h["2ndJetJet_pT"],  "j1pT",    jpTbin);

      book(_h["2ndbJet_pT"], "2ndbJet_pT", jpTbin);
      book(_h["i2b_2ndbm"], "i1b_2ndbm", jpTbin);

      book(_h["ZJ_pTRatio"],      "ZJpT"     , 30, 0, 3);
      book(_h["ZJ_pTRatio_loDR"], "ZJpT_loDR", 30, 0, 3);
      book(_h["ZJ_pTRatio_hiDR"], "ZJpT_hiDR", 30, 0, 3);

      book(_h["DR"], "DR", deltaRbin);
      book(_h["HT"], "HT", jpTbin);

      book(_h["mjj"],      "mjj",      jpTbin);
      book(_h["mjj_loDR"], "mjj_loDR", jpTbin);
      book(_h["mjj_hiDR"], "mjj_hiDR", jpTbin);

      book(_h["jjDPhi"],      "dPhijj",     deltaPhibin);
      book(_h["jjDPhi_loDR"], "dPhijj_loDR", deltaPhibin);
      book(_h["jjDPhi_hiDR"], "dPhijj_hiDR", deltaPhibin);
      
      book(_h["ZpTjet"], "ZpTjet", ZpTbin);
      
      /////////////////////////////////////////////////////////////////

      book(_h["i1b_ZpT"], "i1b_ZpT", ZpTbin);
      book(_h["i1b_ZY"],"i1b_ZY", deltaYbin);;
      book(_h["i1b_dPhiZb"],"i1b_dPhiZb", deltaPhibin);
      book(_h["i1b_dRZb"],"i1b_dRZb", deltaRbin);
      book(_h["i1b_dYZb"],"i1b_dYZb", deltaYbin);
      book(_h["i1b_MZb"],"i1b_MZb", Zmassbin);
      book(_h["i1b_bpT"],"i1b_bpT",  jpTbin);
      book(_h["i1b_bm"],"i1b_bm", jpTbin);
      book(_h["i1b_bY"],"i1b_bY",deltaYbin);
      book(_h["i1b_Xf"],"i1b_Xf", xFbins);;
      book(_h["i2b_ZpT"],"i2b_ZpT", ZpTbin);
      book(_h["i2b_dPhibb"],"i2b_dPhibb", deltaPhibin);
      book(_h["i2b_dRbb"],"i2b_dRbb",deltaRbin);
      book(_h["i2b_dYbb"],"i2b_dYbb", deltaYbin);
      book(_h["i2b_Mbb"],"i2b_Mbb", jpTbin);
      book(_h["i2b_pTbb"],"i2b_pTbb", jpTbin);
      book(_h["i2b_pTOnMbb"],"i2b_pTOnMbb",std::vector<double>{10.,20.,30.,40.,50.,60., 80., 100., 120., 140., 160., 180., 200., 240., 280., 340., 400.});
      book(_h["pTb2OnpTb1b2"],"pTb2OnpTb1b2",std::vector<double>{0,0.05,0.1,0.15,0.2,0.25,0.3,0.35,0.4,0.45,0.5});
//      book(_h["ib_nBJets"],1,15,1);

//////////////////////////////////////////////////

    }

    /// Perform the per-event analysis
    void analyze(const Event& event) {

      // Abnormally large Event weight in Sherpa2.2.1
      if (std::abs(event.weights()[0]) > 100.) vetoEvent;
      
      // access fiducial electrons and muons
      const Particle *l1 = nullptr, *l2 = nullptr;
      auto muons = apply<DressedLeptons>(event, "DressedMuons").dressedLeptons();
      auto elecs = apply<DressedLeptons>(event, "DressedElectrons").dressedLeptons();

      // Dilepton selection 
      if (muons.size()+elecs.size()!=2) vetoEvent;
      if      (muons.size()==2) { l1=&muons[0]; l2=&muons[1]; }
      else if (elecs.size()==2) { l1=&elecs[0]; l2=&elecs[1]; }
//      else if (elecs.size()+muons.size()==2) { l1=&elecs[0]; l2=&muons[0]; }
      else vetoEvent;

      if (_mode == 2 && muons.size() !=2) vetoEvent;
      if (_mode == 1 && elecs.size() !=2) vetoEvent;

      // Dilepton selection :: Z mass peak.
      if ( !inRange((l1->mom()+l2->mom()).mass()/GeV, 76.0, 106.0) ) vetoEvent;

      // lepton-jet overlap removal (note: muons are not included in jet finding)
      // Jets eta < 2.5, pT > 30GeV for overlap removal
	    Jets jets;
	    const Jets& alljets = apply<JetAlg>(event, "jets").jetsByPt(Cuts::pT>20*GeV && Cuts::absrap < 2.5 );
      // Use jets with dR > 0.4 of lepton
	    for (const Jet& j : alljets) {
	    	bool keep = true;
		    for (const  Particle& l : elecs) keep &= deltaR(j, l) > 0.4;
		    for (const  Particle& l : muons) keep &= deltaR(j, l) > 0.4;
		    if (keep)  jets += j;
	    }

      // Calculate the observables
      double HT = 0.;
      double jet0pT = 0.;
      double cljetpT = 0.;
      double minDR = 99.;

      // Dilepton
      auto ll = (l1->mom() + l2->mom());
      double ZpT = ll.pT();
      double Zm  = ll.mass(); 
      double Zphi = ll.phi();
      double Zy = ll.rapidity();
      double Zeta = ll.eta();

//      if (apply<MissingMomentum>(event, "MET").missingPt() > 60*GeV && ZpT < 150*GeV) vetoEvent;
      std::cout << "Event: " << event.genEvent()->event_number() << ", ZMass: " << Zm/GeV << std::endl;
      _h["ZMass"]->fill(Zm/GeV);

      // Require bjets to be above 5 GeV for ghost matching
      std::vector<Jet> bJets;
      for (auto j:jets) {
	const bool isBtagged = j.bTagged(Cuts::pT > 5*GeV);
        if (isBtagged) {
          HT += j.pT();
          bJets.push_back(j);
        }
      }
      const size_t Nbjets = bJets.size();
      // Exclusive NbJets, jet pT > 20  GeV
      std::cout << "Event: " << event.genEvent()->event_number() << ", NbJets_excl: " << Nbjets << std::endl;
      _h["NbJets_excl"]->fill(Nbjets);
      // Inclusive NbJets, jet pT > 20 GeV
      for(size_t i = 0; i <= Nbjets; ++i) { 
         std::cout << "Event: " << event.genEvent()->event_number() << ", NbJets_incl: " << i << std::endl;
        _h["NbJets_incl"]->fill(i);
      }

      // NJets >= 1      
      if (Nbjets < 1) vetoEvent;

      float Zbm = (ll+bJets[0].momentum()).mass()/GeV;
///////////////////////////////////
//      _h["ib_nBJets"]->fill(1);

      double dYZb=fabs(Zy-bJets[0].rapidity());
      double dEtaZb=fabs(Zeta-bJets[0].eta());
      double dPhiZb=acos(cos(fabs(Zphi-bJets[0].phi())));
      double dRZb  =sqrt(dEtaZb*dEtaZb+dPhiZb*dPhiZb);
      double Xf = bJets[0].pT()*sinh(bJets[0].eta())/(13000); 

      std::cout << "Event: " << event.genEvent()->event_number() << ", i1b_ZpT: " << ZpT/GeV << std::endl;
      _h["i1b_ZpT"]->fill(ZpT/GeV);
      std::cout << "Event: " << event.genEvent()->event_number() << ", i1b_ZY: " << fabs(Zy) << std::endl;
      _h["i1b_ZY"]->fill(fabs(Zy));
      std::cout << "Event: " << event.genEvent()->event_number() << ", i1b_dPhiZb: " << dPhiZb << std::endl;
      _h["i1b_dPhiZb"]->fill(dPhiZb);
      std::cout << "Event: " << event.genEvent()->event_number() << ", i1b_dRZb: " << dRZb << std::endl;
      _h["i1b_dRZb"]->fill(dRZb);
      std::cout << "Event: " << event.genEvent()->event_number() << ", i1b_dYZb: " << dYZb << std::endl;
      _h["i1b_dYZb"]->fill(dYZb);
      std::cout << "Event: " << event.genEvent()->event_number() << ", i1b_MZb: " << Zbm << std::endl;
      _h["i1b_MZb"]->fill(Zbm);

      std::cout << "Event: " << event.genEvent()->event_number() << ", i1b_bpT: " << bJets[0].pT()/GeV << std::endl;
      _h["i1b_bpT"]->fill(bJets[0].pT()/GeV);
      std::cout << "Event: " << event.genEvent()->event_number() << ", i1b_bm: " << bJets[0].mass()/GeV << std::endl;
      _h["i1b_bm"]->fill(bJets[0].mass()/GeV);

      std::cout << "Event: " << event.genEvent()->event_number() << ", i1b_bY: " << bJets[0].absrapidity() << std::endl;
      _h["i1b_bY"]->fill(bJets[0].absrapidity());
      std::cout << "Event: " << event.genEvent()->event_number() << ", i1b_Xf: " << Xf << std::endl;
      _h["i1b_Xf"]->fill(Xf);
////////////////////////////////////////////////
      if ( Nbjets >1 ) {
	
	
//     	_h["ib_nBJets"]->fill(2,weight); //inclusive 2-b
	float dYbb   = fabs(bJets[0].rapidity()-bJets[1].rapidity());
	float dPhibb = acos(cos(fabs(bJets[0].phi()-bJets[1].phi())));
	float dRbb   = deltaR(bJets[0], bJets[1]) ;
	float Mbb    = (bJets[0].momentum() + bJets[1].momentum()).mass()/GeV;
	float Ptbb   = (bJets[0].momentum() + bJets[1].momentum()).pt()/GeV;
	
   std::cout << "Event: " << event.genEvent()->event_number() << ", i2b_ZpT: " << ZpT/GeV << std::endl;
	_h["i2b_ZpT"]    ->fill(ZpT/GeV);
   std::cout << "Event: " << event.genEvent()->event_number() << ", i2b_dPhibb: " << dPhibb << std::endl;
	_h["i2b_dPhibb"] ->fill(dPhibb);
  std::cout << "Event: " << event.genEvent()->event_number() << ", i2b_dRbb: " << dRbb << std::endl;
	_h["i2b_dRbb"]   ->fill(dRbb);  
  std::cout << "Event: " << event.genEvent()->event_number() << ", i2b_dYbb: " << dYbb << std::endl;
	_h["i2b_dYbb"]   ->fill(dYbb);
  std::cout << "Event: " << event.genEvent()->event_number() << ", i2b_Mbb: " << Mbb/GeV << std::endl;
	_h["i2b_Mbb"]    ->fill(Mbb/GeV);
  std::cout << "Event: " << event.genEvent()->event_number() << ", i2b_pTbb: " << Ptbb/GeV << std::endl;
	_h["i2b_pTbb"]   ->fill(Ptbb/GeV);
 std::cout << "Event: " << event.genEvent()->event_number() << ", i2b_pTOnMbb: " << Ptbb/Mbb << std::endl;
	_h["i2b_pTOnMbb"]->fill(Ptbb/Mbb);
      }

/////////////////////////////////////////////////////////

      // Z pT
      std::cout << "Event: " << event.genEvent()->event_number() << ", ZpTjet: " << ZpT/GeV << std::endl;
      _h["ZpTjet"]->fill(ZpT/GeV);

      // HT
      HT += ZpT ;
      std::cout << "Event: " << event.genEvent()->event_number() << ", HT: " << HT/GeV << std::endl;
      _h["HT"]->fill(HT/GeV);

      //Leading jet kinematics
      jet0pT = bJets[0].pT();
      std::cout << "Event: " << event.genEvent()->event_number() << ", leadingJet_pT: " << jet0pT/GeV << std::endl;
      _h["leadingJet_pT"]->fill(jet0pT/GeV);
      
      // Z/J pT ratio
      std::cout << "Event: " << event.genEvent()->event_number() << ", ZJ_pTRatio: " << ZpT/jet0pT << std::endl;
      _h["ZJ_pTRatio"]->fill(ZpT/jet0pT);

      // Kinematics of 2 leading jets
      if (Nbjets > 1) {
      std::cout << "Event: " << event.genEvent()->event_number() << ", 2ndbJet_pT: " << bJets[1].pT()/GeV << std::endl;
        _h["2ndbJet_pT"]->fill(bJets[1].pT()/GeV);
      std::cout << "Event: " << event.genEvent()->event_number() << ", i2b_2ndbm: " << bJets[1].mass()/GeV << std::endl;
	_h["i2b_2ndbm"]->fill(bJets[1].mass()/GeV);
      std::cout << "Event: " << event.genEvent()->event_number() << ", pTb2OnpTb1b2: " << bJets[1].pT()/(bJets[0].pT()+bJets[1].pT()) << std::endl;
        _h["pTb2OnpTb1b2"]->fill(bJets[1].pT()/(bJets[0].pT()+bJets[1].pT()));

        // Dijet mass
        std::cout << "Event: " << event.genEvent()->event_number() << ", mjj: " << (bJets[0].mom()+bJets[1].mom()).mass()/GeV << std::endl;
        _h["mjj"]->fill((bJets[0].mom()+bJets[1].mom()).mass()/GeV);
        // Dijet delta Phi
        std::cout << "Event: " << event.genEvent()->event_number() << ", jjDPhi: " << deltaPhi(bJets[0].mom(), bJets[1].mom())/GeV << std::endl;
        _h["jjDPhi"]->fill(deltaPhi(bJets[0].mom(), bJets[1].mom())/GeV);
      }

      // closest jet to Z
      for (auto j : bJets) {
        if (deltaR( j.mom(), ll ,RAPIDITY) < minDR) {
          minDR = deltaR( j.mom(), ll ,RAPIDITY);
          cljetpT = j.pT();
        }
      }
     std::cout << "Event: " << event.genEvent()->event_number() << ", closestJet_pT: " << cljetpT/GeV << std::endl;
      _h["closestJet_pT"]->fill(cljetpT/GeV);
      std::cout << "Event: " << event.genEvent()->event_number() << ", DR: " << minDR << std::endl;
      _h["DR"]->fill(minDR);
      

      // Phase space with DR<2.0       
      if (minDR < 2.0) {
        if (Nbjets > 1) {  
          std::cout << "Event: " << event.genEvent()->event_number() << ", mjj_loDR: " << (bJets[0].mom()+bJets[1].mom()).mass()/GeV << std::endl;
          _h["mjj_loDR"]->fill((bJets[0].mom()+bJets[1].mom()).mass()/GeV);
          std::cout << "Event: " << event.genEvent()->event_number() << ", jjDPhi_loDR: " << deltaPhi(bJets[0].mom(), bJets[1].mom())/GeV << std::endl;
          _h["jjDPhi_loDR"]->fill(deltaPhi(bJets[0].mom(), bJets[1].mom())/GeV);
        }
       std::cout << "Event: " << event.genEvent()->event_number() << ", NbJets_loDR: " << Nbjets << std::endl;
        _h["NbJets_loDR"]->fill(Nbjets);
        std::cout << "Event: " << event.genEvent()->event_number() << ", ZJ_pTRatio_loDR: " << ZpT/jet0pT << std::endl;
        _h["ZJ_pTRatio_loDR"]->fill(ZpT/jet0pT);
      } else {
        if (Nbjets > 1) {  
          std::cout << "Event: " << event.genEvent()->event_number() << ", mjj_hiDR: " << (bJets[0].mom()+bJets[1].mom()).mass()/GeV << std::endl;
          _h["mjj_hiDR"]->fill((bJets[0].mom()+bJets[1].mom()).mass()/GeV);
         std::cout << "Event: " << event.genEvent()->event_number() << ", jjDPhi_hiDR: " << deltaPhi(bJets[0].mom(), bJets[1].mom())/GeV << std::endl;
          _h["jjDPhi_hiDR"]->fill(deltaPhi(bJets[0].mom(), bJets[1].mom())/GeV);
        }
        std::cout << "Event: " << event.genEvent()->event_number() << ", NbJets_hiDR: " << Nbjets << std::endl;
        _h["NbJets_hiDR"]->fill(Nbjets);
        std::cout << "Event: " << event.genEvent()->event_number() << ", ZJ_pTRatio_hiDR: " << ZpT/jet0pT << std::endl;
        _h["ZJ_pTRatio_hiDR"]->fill(ZpT/jet0pT);
      }

    }

    void finalize() {
      double xsec = crossSection()/picobarn/sumW();
      if (_mode == 0) xsec = xsec * 0.5;
	  
      // Normalize to cross section.
      scale(_h["ZMass"],xsec);
      scale(_h["NJets_incl"],xsec);
      scale(_h["NJets_excl"],xsec);
      scale(_h["NJets_loDR"],xsec);
      scale(_h["NJets_hiDR"],xsec);
      scale(_h["leadingJet_pT"],xsec);
      scale(_h["closestJet_pT"],xsec);
      scale(_h["2ndJet_pT"],xsec);
      scale(_h["ZJ_pTRatio"],xsec);
      scale(_h["ZJ_pTRatio_loDR"],xsec);
      scale(_h["ZJ_pTRatio_hiDR"],xsec);
      scale(_h["DR"],xsec);
      scale(_h["HT"],xsec);
      scale(_h["mjj"],xsec);
      scale(_h["mjj_loDR"],xsec);
      scale(_h["mjj_hiDR"],xsec);
      scale(_h["jjDPhi"],xsec);
      scale(_h["jjDPhi_loDR"],xsec);
      scale(_h["jjDPhi_hiDR"],xsec);
      scale(_h["ZpTjet"],xsec);
////////////////////////////////////////////////////////////////////
      
      scale(_h["i1b_ZpT"],xsec);
      scale(_h["i1b_ZY"],xsec);
      scale(_h["i1b_dPhiZb"],xsec);
      scale(_h["i1b_dRZb"],xsec);
      scale(_h["i1b_dYZb"],xsec);
      scale(_h["i1b_MZb"],xsec);

      scale(_h["i1b_bpT"],xsec);
      scale(_h["i1b_bm"],xsec);
      scale(_h["i1b_bY"],xsec);
      scale(_h["i1b_Xf"],xsec);

      scale(_h["i2b_ZpT"],xsec);
      scale(_h["i2b_dPhibb"],xsec);
      scale(_h["i2b_dRbb"],xsec);
      scale(_h["i2b_dYbb"],xsec);
      scale(_h["i2b_Mbb"],xsec);
      scale(_h["i2b_pTbb"],xsec);
      scale(_h["i2b_pTOnMbb"],xsec);
      scale(_h["pTb2OnpTb1b2"],xsec);

      scale(_h["2ndbJet_pT"],xsec);
      scale(_h["i2b_2ndbm"],xsec);

//      scale(_h["ib_nBJets"],xsec);
///////////////////////////////////////////////////////////////////
    } // end of finalize

    //@}

    // define histograms
    size_t _mode;
    map<string, Histo1DPtr> _h;
 
  };

  DECLARE_RIVET_PLUGIN(Zbb_rivet);
}
